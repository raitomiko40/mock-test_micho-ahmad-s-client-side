
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import {
  fetchTodos,
  addTodo,
  updateTodo,
  deleteTodo,
} from '../../store/actions/todo';
import { useNavigate } from 'react-router-dom';

function TodoList() {
  const todos = useSelector((state) => state.todo.todos);
  const userId = useSelector((state) => state.user.userId);
  const token = localStorage.getItem('token');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [newTodoTitle, setNewTodoTitle] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (!token) {
          navigate('/');
        } else {
          const response = await axios.get(`https://odd-lime-meerkat-gear.cyclic.app/todos/${userId}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          dispatch(fetchTodos(response.data));
          setLoading(false);
        }
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [userId, token, dispatch, navigate]);

  const createNewTodo = async () => {
    try {
      if (!newTodoTitle) {
        alert('Todo title must be filled out');
        return;
      }

      const response = await axios.post(
        'https://odd-lime-meerkat-gear.cyclic.app/todos',
        {
          title: newTodoTitle,
          complete: false,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      dispatch(addTodo(response.data));
      setNewTodoTitle('');
    } catch (error) {
      console.error('Error creating todo:', error);
    }
  };

  const handleCompleteToggle = async (todo) => {
    try {
      const updatedTodo = {
        ...todo,
        complete: !todo.complete,
      };

      updatedTodo.id = todo.id;

      await axios.put(
        `https://odd-lime-meerkat-gear.cyclic.app/todos/${todo.id}`,
        updatedTodo,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      dispatch(updateTodo(updatedTodo));
    } catch (error) {
      console.error('Error updating todo:', error);
    }
  };

  const deleteTodoItem = async (todoId) => {
    try {
      await axios.delete(`https://odd-lime-meerkat-gear.cyclic.app/todos/${todoId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      dispatch(deleteTodo(todoId));
    } catch (error) {
      console.error('Error deleting todo:', error);
    }
  };

  if (error) {
    return <div>Error fetching todos: {error.message}</div>;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1 className="text-2xl font-bold mb-4 mt-12">Your Tasks</h1>
      <div className="mb-4">
        <input
          type="text"
          placeholder="Enter Task Name"
          value={newTodoTitle}
          onChange={(e) => setNewTodoTitle(e.target.value)}
          className="border rounded-md px-2 py-1 mr-2"
        />
        <button onClick={createNewTodo} className="bg-blue-500 text-white px-4 py-1 rounded-md">
          Create Tasks
        </button>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
        {todos.map((todo) => (
          <div key={todo.id} className="bg-white rounded-lg shadow-md p-4">
            <input
              type="checkbox"
              checked={todo.complete}
              onChange={() => handleCompleteToggle(todo)}
              className="mr-2"
            />
            <strong className={todo.complete ? 'line-through' : ''}>{todo.title}</strong>
            <button onClick={() => deleteTodoItem(todo.id)} className="bg-red-500 text-white px-2 py-1 ml-2 rounded-md">
              Delete
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

export default TodoList;
