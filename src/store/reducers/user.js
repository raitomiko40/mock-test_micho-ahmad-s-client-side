
const initialState = {
    userData: {},
    userId: null,
  };
  
  const userReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'SET_USER_DATA':
        return {
          ...state,
          userData: action.payload.userData,
          userId: action.payload.userId,
        };
      default:
        return state;
    }
  };
  
  export default userReducer;
  