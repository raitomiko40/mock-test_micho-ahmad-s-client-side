// src/store/actions/user.js
export const setUserData = (userData, userId) => ({
    type: 'SET_USER_DATA',
    payload: { userData, userId },
  });
  