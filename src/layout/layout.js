import React from 'react';
import Navbar from '../components/Navbar/navbar';

function Layout({ children }) {
  return (
    <div>
      <Navbar />
      <div className="container mx-auto p-4">
        {children}
      </div>
    </div>
  );
}

export default Layout;
