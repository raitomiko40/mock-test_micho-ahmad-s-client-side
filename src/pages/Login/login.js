import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

function Login() {
  const navigate = useNavigate();
  const [idLogin, setIdLogin] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    const isAuthenticated = localStorage.getItem('token');
    if (isAuthenticated) {
      navigate('/home'); 
    }
  }, [navigate]);

  const handleLogin = async () => {
    try {
      const response = await axios.post('https://odd-lime-meerkat-gear.cyclic.app/auth/login', { idLogin });
      const { token } = response.data;

      localStorage.setItem('token', token);
      navigate('/home'); 
    } catch (error) {
      setErrorMessage('Login gagal. Silakan coba lagi.');
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Login</h2>
        </div>
        <form className="mt-8 space-y-6" onSubmit={(e) => e.preventDefault()}>
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <input
                id="idLogin"
                name="idLogin"
                type="text"
                placeholder="ID Login"
                value={idLogin}
                onChange={(e) => setIdLogin(e.target.value)}
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              />
            </div>
          </div>

          {errorMessage && (
            <div className="text-red-500 text-sm font-medium">{errorMessage}</div>
          )}

          <div>
            <button
              type="submit"
              onClick={handleLogin}
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Login
            </button>
          </div>

          <div className="mt-4 text-center">
            Belum punya akun?{' '}
            <span
              onClick={() => navigate('/register')}
              className="cursor-pointer text-indigo-600 hover:underline"
            >
              Sign Up here!
            </span>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
