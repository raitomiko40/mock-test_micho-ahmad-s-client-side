import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Layout from '../../layout/layout';
import TodoList from '../../components/HomeComp/todoList';
import { setUserData } from '../../store/actions/user';

function Home() {
  const userData = useSelector((state) => state.user.userData);
  const userId = useSelector((state) => state.user.userId);
  const token = localStorage.getItem('token');
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!token) {
      navigate('/');
    } else {
      axios
        .get('https://odd-lime-meerkat-gear.cyclic.app/user', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          dispatch(setUserData(response.data.data, response.data.data.id));
        })
        .catch((error) => {
          console.error('Error fetching user data', error);
        });
    }
  }, [token, navigate, dispatch]);

  return (
    <Layout>
      <div className="container mx-auto mt-4">
        <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <h2 className="text-2xl font-bold mb-4">Welcome to the Home Page</h2>
          {userData && Object.keys(userData).length > 0 ? (
            <div>
              <p className="mt-2">Name: {userData.name}</p>
              <p className="mt-2">Email: {userData.email}</p>
              <p className="mt-2">Address: {userData.address}</p>
            </div>
          ) : (
            <p>Loading user data...</p>
          )}

          {userId !== null ? (
            <TodoList userId={userId} token={token} />
          ) : (
            <p>Loading todo list...</p>
          )}
        </div>
      </div>
    </Layout>
  );
}

export default Home;
