import React from 'react';
import { Provider } from 'react-redux'; 
import store from './store'; 
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Login from './pages/Login/login';
import Register from './pages/Register/register';
import Home from './pages/Home/home';

function App() {
  const isAuthenticated = localStorage.getItem('token')

  return (
    <Provider store={store}> 
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/home" element={isAuthenticated ? <Home /> : <Navigate to="/" />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
